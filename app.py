from flask import Flask
from flask import render_template
app = Flask(__name__)

wifibot = dict()
wifibot["speed"] = 42

@app.route('/')
def main(name=None):
    return render_template("fluid.html")

@app.route("/speed", methods=['GET'])
def speed():
    return wifibot["speed"]

@app.route("/move/stop")
def stop():
    return wifibot.stop()

@app.route("/move/rotate/<angle>", methods=["POST"])
def rotate(angle):
    return wifibot.rotate(angle)

@app.route("/move/go/<distance>", methods=["POST"])
def go(distance):
    return wifibot.go(distance)

if __name__ == "__main__":
    app.run(debug=True)
